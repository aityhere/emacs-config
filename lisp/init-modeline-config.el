;;;; coding:  utf-8
;;;; author： fengzp
;;;; version: 2.0
;;;; data:    2022-09
;;;; desc:    状态栏配置
;;;;

(message "开始加载状态栏配置......")

;;powerline支持
(use-package powerline
  :config
  (package-default-theme))

;;显示文件行、列、大小 ，美化状态栏
(use-package simple
  :ensure nil
  :hook (after-init . size-indication-mode)
  :init
  (progn
    (setq column-number-mode t)
    ))

;;在modeline上显示按键和执行的命令
(use-package keycast
  :init
  (add-to-list 'global-mode-string '("" mode-line-keycast))
  (keycast-mode))

;; 状态栏美化扩展组件, 这里的执行顺序非常重要，doom-modeline-mode 的激活时机一定要在设置global-mode-string 之后‘
(use-package doom-modeline
  :ensure t
  :hook (emacs-startup . doom-modeline-mode)
  :after keycast
  :config
  (display-time-mode 1)
  (display-battery-mode 1))

;; 使用状态栏闪烁的方式提示错误
(setq visible-bell nil
      ring-bell-function 'double-flash-mode-line)
(defun double-flash-mode-line ()
  (let ((flash-sec (/ 1.0 20)))
    (invert-face 'mode-line)
    (run-with-timer flash-sec nil #'invert-face 'mode-line)
    (run-with-timer (* 2 flash-sec) nil #'invert-face 'mode-line)
    (run-with-timer (* 3 flash-sec) nil #'invert-face 'mode-line)))




(message "状态栏配置加载完成")
(provide 'init-modeline-config)
