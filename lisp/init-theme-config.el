;;;;
;;;;


(message "开始加载主题配置......")

;; Emacs 不再额外对 Theme 的代码安全性确认提示
(setq custom-safe-themes t)

;;主题
(when *is-mac*
  (use-package auto-dark-emacs
    :config
    (setq auto-dark-emacs/dark-theme 'doom-one)
    (setq auto-dark-emacs/light-theme 'doom-solarized-light)
    (setq auto-dark-emacs/polling-interval-seconds 60)
    (auto-dark-emacs/check-and-set-dark-mode)))

(when *is-windows*
  (use-package monokai-theme
    :init (load-theme 'monokai t)))

;; (use-package smart-mode-line
;;   :init
;;   (setq sml/no-confirm-load-theme t)
;;   (setq sml/theme 'respectful)
;;   (sml/setup))



(message "主题配置加载完成")
(provide 'init-theme-config)
