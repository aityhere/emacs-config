;;;; coding:  utf-8
;;;; author： fengzp
;;;; version: 2.0
;;;; data:    2022-09
;;;; desc:    包管理器配置
;;;;

(message "开始加载包管理器......")

(require 'package)
(setq package-check-signature nil
      load-prefer-newer t)
(setq package-archives '(("gnu"   . "http://mirrors.cloud.tencent.com/elpa/gnu/")
                         ("melpa" . "http://mirrors.cloud.tencent.com/elpa/melpa/")))

(unless (bound-and-true-p package--initialized) ;; To avoid warnings on 27
  (package-initialize))

(unless package-archive-contents
  (package-refresh-contents))

;; settings for use-package package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;;设置use-package包全局配置
(eval-and-compile
  (setq use-package-always-defer t ;默认都是延迟加载，不用每个包都手动添加:defer t
	use-package-always-ensure t ;不用每个包都手动添加:ensure t
	use-package-always-demand nil
	use-package-expand-minimally t
	use-package-verbose t)
  (require 'use-package))

(use-package auto-package-update
  :init (setq auto-package-update-delete-old-versions t
        auto-package-update-hide-results t))

;;通过 benchmark-init 包进行启动耗时的测量
;;M-x benchmark-init/show-durations-tree 或者 M-x benchmark-init/show-durations-tabulated
(use-package benchmark-init 
  :init (benchmark-init/activate) 
  :hook (after-init . benchmark-init/deactivate))

;; (use-package use-package
;;   :ensure t  ;是否一定要确保安装
;;   :defer t   ;是否要延迟加载
;;   :init (setq xxx-scrolling-margin 2)   ;初始化参数
;;   :config (xxx-scrolling-mode t) ;基本配置参数
;;   :bind      ;快捷键绑定
;;   :hook      ;hook模式的绑定
;;   )

(message "包管理器加载完成")
(provide 'init-package)
