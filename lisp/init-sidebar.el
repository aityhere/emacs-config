
(message "开始加载侧边栏......")
;; 侧边栏文件浏览器
;; (use-package dired-sidebar
;;   :ensure t
;;   :commands (dired-sidebar-toggle-sidebar)
;;   :bind (("C-x C-n" . dired-sidebar-toggle-sidebar))
;;   :init
;;   (add-hook 'dired-sidebar-mode-hook
;;             (lambda ()
;;               (unless (file-remote-p default-directory)
;;                 (auto-revert-mode))))
;;   (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
;;   :config
;;   (push 'toggle-window-split dired-sidebar-toggle-hidden-commands)
;;   (push 'rotate-windows dired-sidebar-toggle-hidden-commands)

;;   (setq dired-sidebar-subtree-line-prefix "__")
;;   (setq dired-sidebar-theme 'vscode)
;;   (setq dired-sidebar-use-term-integration t)
;;   (setq dired-sidebar-use-custom-font t))

;; (use-package ibuffer-sidebar
;;   :ensure t
;;   :commands (ibuffer-sidebar-toggle-sidebar)
;;   :config
;;   ;; (setq ibuffer-sidebar-face `(:family "Helvetica" :height 140))
;;   (setq ibuffer-sidebar-use-custom-font t))

;; (use-package all-the-icons-dired
;;   :after all-the-icons)


(message "侧边栏加载完成")
(provide 'init-sidebar)
