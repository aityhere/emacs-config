;;;; coding:  utf-8
;;;; author： fengzp
;;;; version: 2.0
;;;; data:    2022-09
;;;; desc:    ivy专项配置
;;;;

(message "开始加载ivy专项配置包......")

;;文本编辑器强化搜索
(use-package ivy
  :ensure t
  :diminish (ivy-mode)
  :hook (after-init . ivy-mode)
  :bind (("C-x b" . ivy-switch-buffer))
  :init
  (ivy-mode)
  :config
  (setq ivy-use-virtual-buffers t)
  (setq ivy-use-selectable-prompt t)
  (setq ivy-count-format "%d/%d ")
  (setq ivy-display-style 'fancy)
  (setq enable-recursive-minibuffers t)
					;(setq ivy-re-builders-alist '((t . ivy-regex-ignore-order)))
  (setq ivy-initial-inputs-alist '((counsel-minor . "^+")
				   (counsel-package . "^+")
				   (counsel-org-capture . "^")
				   (counsel-M-x . "^")
				   (counsel-describe-symbol . "^")
				   (org-refile . "^")
				   (org-agenda-refile . "^")
				   (org-capture-refile . "^")
				   (Man-completion-table . "^")
				   (woman . "^")
				   (ryb-register . "^")
				   (counsel-register . "^"))))


(use-package counsel
  :ensure t
  :diminish counsel-mode
  :after ivy
  :bind (("M-r" . counsel-register)
	 ("M-y" . counsel-yank-pop)
	 ("C-x d" . counsel-dired)
	 ("M-x" . counsel-M-x)
	 ("C-x C-f" . counsel-find-file)
	 ("C-c f" . counsel-recentf)
	 ("C-c g" . counsel-git)
	 :map ivy-minibuffer-map ("M-y" . ivy-next-line))
  :init
  (counsel-mode)
  :config
  (setq counsel-dired-listing-switches "-alhtgG"))

(use-package ivy-avy
  :ensure t)

(use-package swiper
  :ensure t
  :bind (("C-s" . swiper)
	 ("M-x" . counsel-M-x)
	 ("C-t" . isearch-forward)
	 ("C-x C-f" . counsel-find-file))
  :config
  (progn
    (setq ivy-use-virtual-buffers t)
    (setq swiper-action-recenter t)
    (setq swiper-include-line-number-in-search t)
    ;; (setq ivy-display-style 'fancy)
    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)))

(use-package avy
  :ensure t
  :bind (("M-s" . avy-goto-word-1)
	 ("M-S" . avy-goto-char))
  :config
  (setq avy-all-windows nil)
  (setq avy-line-insert-style 'below)
  (setq avy-orders-alist '((avy-goto-char . avy-order-closest)
			   (avy-goto-word-1 . avy-order-closest))))


;; https://github.com/oantolin/orderless
;; 使用“空格”分割不同关键字，关键词出现顺序无关，完成候选项的快速查找和选取
(use-package orderless
  :ensure t
  :after ivy
  :config
  (setq ivy-re-builders-alist '((t . orderless-ivy-re-builder)))
  (add-to-list 'ivy-highlight-functions-alist '(orderless-ivy-re-builder . orderless-ivy-highlight))
  )

;; From https://github.com/daviwil/emacs-from-scratch/blob/master/show-notes/Emacs-Tips-Prescient.org
(use-package ivy-prescient
  :ensure t
  ;; :after counsel
  :config
  (ivy-prescient-mode 1)
  ;; Remember candidate frequencies across sessions
  (prescient-persist-mode 1))


(use-package all-the-icons-ivy-rich
  :ensure t
  :init (all-the-icons-ivy-rich-mode 1))

(use-package ivy-rich
  :ensure t
  :init (ivy-rich-mode 1))


;;minibuffer位置
(use-package ivy-posframe
  :ensure t
  :diminish ivy-posframe-mode
  :init (ivy-posframe-mode 1)
  :config
  (setq ivy-posframe-display-functions-alist
	'((swiper . ivy-posframe-display-at-frame-center)
	  (complete-symbol . ivy-posframe-display-at-point)
	  (counsel-M-x . ivy-posframe-display-at-frame-center)
	  (counsel-find-file . ivy-posframe-display-at-frame-center)
	  (ivy-switch-buffer . ivy-posframe-display-at-frame-center)
	  (t . ivy-posframe-display-at-frame-center)))
  (setq ivy-posframe-parameters
	'((left-fringe . 8)
          (right-fringe . 8)))
  (setq ivy-posframe-border-width 2))




(message "ivy专项配置加载完成")
(provide 'init-ivy-config)
