;;;; coding: utf-8
;;;; author：fengzp
;;;; version: 2.0
;;;; data: 2022-09
;;;; desc: 工具定义，包含常量、变量、函数
;;;;

(message "开始加载工具配置......")

(defconst *is-windows* (or (eq system-type 'ms-dos) (eq system-type 'windows-nt)) "是否是Windows系统")
(defconst *is-mac* (eq system-type 'darwin) "是否是Mac系统")
(defconst *is-linux* (eq system-type 'gnu/linux) "是否是Linux系统")

(defconst *is-before-emacs-23* (>= 23 emacs-major-version) "是否是emacs 23或以前的版本")
(defconst *is-after-emacs-27*  (<= 27 emacs-major-version) "是否是emacs 27或以后的版本")
(defconst *is-after-emacs-24*  (<= 24 emacs-major-version) "是否是emacs 24或以后的版本")

(message "工具配置加载完成")

(provide 'init-utils)
