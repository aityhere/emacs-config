

(message "开始加载临时配置......")

(use-package counsel 
  :ensure t
  :diminish counsel-mode
  :after ivy
  :bind (("M-r" . counsel-register)
	 ("M-y" . counsel-yank-pop)
	 ("C-x d" . counsel-dired)
	 :map ivy-minibuffer-map ("M-y" . ivy-next-line))
  :init
  (counsel-mode)
  :config
  (setq counsel-dired-listing-switches "-alhtgG"))

(use-package ivy 
  :ensure t 
  :diminish (ivy-mode)
  :bind (("C-x b" . ivy-switch-buffer))
  :init
;  (ivy-mode)
  :config
  (setq ivy-use-virtual-buffers t) 
  (setq ivy-count-format "%d/%d ") 
  (setq ivy-display-style 'fancy)
  (setq ivy-initial-inputs-alist '((counsel-minor . "^+")
				   (counsel-package . "^+")
				   (counsel-org-capture . "^")
				   (counsel-M-x . "^")
				   (counsel-describe-symbol . "^")
				   (org-refile . "^")
				   (org-agenda-refile . "^")
				   (org-capture-refile . "^")
				   (Man-completion-table . "^")
				   (woman . "^")
				   (ryb-register . "^")
				   (counsel-register . "^"))))

(use-package ivy-avy
  :ensure t)

(use-package swiper 
  :ensure t 
  :bind (("C-s" . swiper) 
	 ("M-x" . counsel-M-x)
	 ("C-t" . isearch-forward)
	 ("C-x C-f" . counsel-find-file)) 
  :config
  (progn 
    (setq ivy-use-virtual-buffers t) 
    ;; (setq ivy-display-style 'fancy) 
    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)))

(use-package avy 
  :ensure t 
  :bind (("M-s" . avy-goto-word-1)
	 ("M-S" . avy-goto-char))
  :config
  (setq avy-all-windows nil)
  (setq avy-line-insert-style 'below)
  (setq avy-orders-alist '((avy-goto-char . avy-order-closest)
			   (avy-goto-word-1 . avy-order-closest))))


(use-package ivy-posframe
  :ensure t
  :diminish ivy-posframe-mode  
  :init (ivy-posframe-mode 1)
  :config
  ;(setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display-at-frame-bottom-left)))
  (setq ivy-posframe-display-functions-alist
  '((swiper . ivy-posframe-display-at-frame-center)
    (complete-symbol . ivy-posframe-display-at-point)
    (counsel-M-x . ivy-posframe-display-at-frame-center)
    (counsel-find-file . ivy-posframe-display-at-frame-center)
    (ivy-switch-buffer . ivy-posframe-display-at-frame-center)
    (t . ivy-posframe-display-at-frame-center)))
  (setq ivy-posframe-parameters
	 '((left-fringe . 8)
          (right-fringe . 8)))
  (setq ivy-posframe-border-width 2))



(message "临时配置加载完成")
(provide 'init-test)
