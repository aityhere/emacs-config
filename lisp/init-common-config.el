;;;; coding:  utf-8
;;;; author： fengzp
;;;; version: 2.0
;;;; data:    2022-09
;;;; desc:    常用配置
;;;;

(message "开始加载常用配置......")

(electric-pair-mode t)                       ; 自动补全括号
(add-hook 'prog-mode-hook #'show-paren-mode) ; 编程模式下，光标在括号上时高亮另一个括号
(column-number-mode t)                       ; 在 Mode line 上显示列号
(global-auto-revert-mode t)                  ; 当另一程序修改了文件时，让 Emacs 及时刷新 Buffer
(delete-selection-mode t)                    ; 选中文本后输入文本会替换文本（更符合我们习惯了的其它编辑器的逻辑）
(setq inhibit-startup-message t)             ; 关闭启动 Emacs 时的欢迎界面
(setq make-backup-files nil)                 ; 关闭文件自动备份
(setq-default make-backup-files nil)         ;不要生成临时文件
(setq backup-inhibied t)                     ;不产生备份
(setq auto-save-default nil)                 ;不生成名为#filename#的临时文件
(add-hook 'prog-mode-hook #'hs-minor-mode)   ; 编程模式下，可以折叠代码块
(global-display-line-numbers-mode 1)         ; 在 Window 显示行号
(tool-bar-mode -1)                           ; 关闭 Tool bar
(when (display-graphic-p) (toggle-scroll-bar -1)) ; 图形界面时关闭滚动条
(setq default-fill-column 120)                ;;把 fill-column 设为 120. 这样的文字更好读。
(global-hl-line-mode 1)                       ;;高亮显示光标所在行

(blink-cursor-mode -1)

(setq tab-width 4)                            ;;设置tab键为4个空格
(setq-default indent-tabs-mode nil)           ;;使用空格代替TAB


(setq visible-bell t)                        ;;取消警告声音
(fset 'yes-or-no-p 'y-or-n-p)                ;不要总是没完没了的问yes or no, 为什么不能用 y/n
(icomplete-mode 1)                           ;用M-x执行某个命令的时候，在输入的同时给出可选的命令名提示
(setq initial-scratch-message "")            ;初始化 scratch buffer 为空(不显示里面的注释)
;; 设置时间戳，标识出最后一次保存文件的时间。
(setq time-stamp-active t)
(setq time-stamp-warn-inactive t)
(setq time-stamp-format "%:y-%02m-%02d %3a %02H:%02M:%02S fengzp")

(setq gc-cons-threshold most-positive-fixnum)

(setq default-directory user-emacs-directory) ;;设置打开的默认目录为~/.emacs.d

;;按照windowz用户的习惯使用 `C-x C-c C-v'
;;(setq cua-mode t)

;;开启tab模式
;;(tab-bar-mode)
;;(tab-line-mode)


(setq user-full-name "冯振平")
(setq user-mail-address "163-12@163.com")

;;设置标题栏
(setq frame-title-format
      (list (format "冯振平 - Emacs@%s %%S: %%j " (system-name))
            '(buffer-file-name "%f" (dired-directory dired-directory "%b"))))

;; (defun frame-title-string ()
;;   "Return the file name of current buffer, using ~ if under home directory"
;;   (let
;;       ((fname (or
;; 	       (buffer-file-name (current-buffer))
;; 	       (buffer-name)))
;;        (max-len 100))
;;     (when (string-match (getenv "HOME") fname)
;;       (setq fname (replace-match "~" t t fname)))
;;     (if (> (length fname) max-len)
;; 	(setq fname
;; 	      (concat "..."
;; 		      (substring fname (- (length fname) max-len)))))
;;     fname))
;; ;;设置标题
;; (setq frame-title-format '("冯振平 - Emacs@"(:eval (frame-title-string))))

(defun kill-shell  ()
  (set-process-sentinel (get-buffer-process (current-buffer))
                        #'kill-shell-buffer-on-exit))
(defun kill-shell-buffer-on-exit (process state)
  (kill-buffer (current-buffer)))
;;退出shell时删除shell buffer
(add-hook 'shell-mode-hook 'kill-shell)

;;注释快捷键
;; (global-set-key (kbd "C-c C-/") 'comment-or-uncomment-region)

;; (defun my-comment-or-uncomment-region (beg end &optional arg)
;;   (interactive (if (use-region-p)
;;                    (list (region-beginning) (region-end) nil)
;;                  (list (line-beginning-position)
;;                        (line-beginning-position 2))))
;;   (comment-or-uncomment-region beg end arg))
;; (global-set-key [remap comment-or-uncomment-region] 'my-comment-or-uncomment-region)


;;Ctrl-Alt-\ 格式化
;; (dolist (command '(yank yank-pop))
;;   (eval
;;    `(defadvice ,command (after indent-region activate)
;;       (and (not current-prefix-arg)
;;            (member major-mode
;;                    '(emacs-lisp-mode
;;                      lisp-mode
;;                      clojure-mode
;;                      scheme-mode
;;                      haskell-mode
;;                      ruby-mode
;;                      rspec-mode
;;                      python-mode
;; 		     java-mode
;;                      c-mode
;;                      c++-mode
;;                      objc-mode
;;                      latex-mode
;;                      js-mode
;;                      plain-tex-mode))
;;            (let ((mark-even-if-inactive transient-mark-mode))
;;              (indent-region (region-beginning) (region-end) nil))))))

(message "常用配置加载完成")
(provide 'init-common-config)
