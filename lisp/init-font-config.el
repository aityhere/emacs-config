;;;; coding:  utf-8
;;;; author： fengzp
;;;; version: 2.0
;;;; data:    2022-09
;;;; desc:    字体配置
;;;;

;;(set-frame-font "-outline-JetBrains Mono Light-light-italic-normal-mono-17-*-*-*-c-*-windows-1258")

(message "开始加载字体配置......")

;; 第一版本
;; (use-package emacs
;;   :if (display-graphic-p)
;;   :config
;;   (require 'init-utils)
;;   (when *is-mac*
;;     (setq fonts '("SF Mono" "冬青黑体简体中文"))
;;     (set-fontset-font t 'unicode "Apple Color Emoji" nil 'prepend)
;;     (set-face-attribute 'default nil :font
;; 			(format "%s:pixelsize=%d" (car fonts) 16)))

;;   (when *is-windows*
;;     (setq fonts '("Consolas" "微软雅黑"))
;;     (set-fontset-font t 'unicode "Segoe UI Emoji" nil 'prepend)
;;     (set-face-attribute 'default nil :font
;; 			(format "%s:pixelsize=%d" (car fonts) 18)))

;;   (when *is-linux*
;;     (setq fonts '("SF Mono" "Noto Sans Mono CJK SC"))
;;     (set-fontset-font t 'unicode "Noto Color Emoji" nil 'prepend)
;;     (set-face-attribute 'default nil :font
;; 			(format "%s:pixelsize=%d" (car fonts) 18)))

;;   (dolist (charset '(kana han symbol cjk-misc bopomofo))
;;     (set-fontset-font (frame-parameter nil 'font) charset
;; 		      (font-spec :family (car (cdr fonts)))))
;;   )

;; 第二版
;; (defun available-font (font-list)
;;   "Get the first available font from FONT-LIST."
;;   (catch 'font
;;     (dolist (font font-list)
;;       (if (member font (font-family-list))
;; 	  (throw 'font font)))))

;; (defun cabins/setup-font ()
;;   "Font setup."
;;   (interactive)
;;   (let* ((efl '("Consolas for Powerline" "Cascadia Code" "Source Code Pro" "JetBrains Mono" "Courier New" "Monaco" "Ubuntu Mono"))
;; 	 (cfl '("DejaVu Sans Mono for Powerline" "楷体" "黑体" "STHeiti" "STKaiti"))
;; 	 (cf (available-font cfl))
;; 	 (ef (available-font efl)))
;;     (when ef
;;       (dolist (face '(default fixed-pitch fixed-pitch-serif variable-pitch))
;; 	(set-face-attribute face nil :family ef)))
;;     (when cf
;;       (dolist (charset '(kana han cjk-misc bopomofo))
;; 	(set-fontset-font t charset cf))
;;       (setq face-font-rescale-alist
;; 	    (mapcar (lambda (item) (cons item 1.2)) cfl)))))


;; (if (daemonp)
;;     (add-hook 'after-make-frame-functions
;; 	      (lambda (frame)
;; 		(with-selected-frame frame
;; 		  (cabins/setup-font))))
;;   (add-hook 'after-init-hook #'cabins/setup-font))


(use-package cnfonts
  :ensure t
  ;;:after all-the-icons
  :hook (cnfonts-set-font-finish
         . (lambda (fontsizes-list)
             (set-fontset-font t 'unicode (font-spec :family "all-the-icons") nil 'append)
             (set-fontset-font t 'unicode (font-spec :family "file-icons") nil 'append)
             (set-fontset-font t 'unicode (font-spec :family "Material Icons") nil 'append)
             (set-fontset-font t 'unicode (font-spec :family "github-octicons") nil 'append)
             (set-fontset-font t 'unicode (font-spec :family "FontAwesome") nil 'append)
             (set-fontset-font t 'unicode (font-spec :family "Weather Icons") nil 'append)))
  :config
  (cnfonts-enable))


(use-package all-the-icons
  :ensure t
  :after cnfonts
  :when (display-graphic-p))



(message "字体配置加载完成")

(provide 'init-font-config)
