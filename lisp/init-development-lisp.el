;;;
;;;
;;;
;;;


(message "开始加载Lisp环境配置......")

(use-package paredit :hook (emacs-lisp-mode . enable-paredit-mode))


(message "Lisp环境配置完成")

(provide 'init-development-lisp)
