;;;; coding:  utf-8
;;;; author： fengzp
;;;; version: 2.0
;;;; data:    2022-09
;;;; desc:    常用配置
;;;;

(message "开始加载常用包......")


;;添加额外第三方包
(use-package gnu-elpa-keyring-update)
(use-package diminish)
(use-package delight)

;;重启emcas
(use-package restart-emacs)

;; Server mode.
;; Use emacsclient to connect
(use-package server
  :ensure nil
  :hook (after-init . server-mode))

;;按照windowz用户的习惯使用 `C-x C-c C-v'
;;(setq cua-mode t)

;;使用maxframe包，使启动时窗口最大化
(use-package maxframe
  :ensure t
  :init
  (add-hook 'window-setup-hook 'maximize-frame t))


;;自动识别文件编码
(use-package unicad
  :ensure t
  :init
  (require 'unicad))


;;emcas常用操作
(use-package crux
  :bind (("C-a" . crux-move-beginning-of-line) ;移动到行首或第一次字符前
	 ("C-c ^" . crux-top-join-line)
	 ("C-S-d" . crux-duplicate-current-line-or-region) ;复制当前行或当前选中的内容
	 ("C-S-k" . crux-smart-kill-line)))  ;删除当前行

;;彩虹括号
(use-package rainbow-delimiters
  :init (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

;; (use-package highlight-parentheses
;;   :init (add-hook 'prog-mode-hook 'highlight-parentheses-mode))


;;高亮选中符号
;; (use-package highlight-symbol
;;   :ensure t
;;   :init (highlight-symbol-mode)
;;   :bind ("<f3>" . highlight-symbol))
;; 按下 F3 键就可高亮当前符号

(use-package symbol-overlay
  :ensure t
  :bind (("M-i" . 'symbol-overlay-put)
	 ("M-n" . 'symbol-overlay-switch-forward)
	 ("M-p" . 'symbol-overlay-switch-backward)
	 ("<f7>" . 'symbol-overlay-mode)
	 ("<f8>" . 'symbol-overlay-remove-all)))

;;平滑滚动
(use-package good-scroll
  :ensure t
  :init (good-scroll-mode))

;;自动补全
(use-package company
  :ensure t
  :init (global-company-mode)
  :config
  (setq company-minimum-prefix-length 1 ; 只需敲 1 个字母就开始进行自动补全
        company-dabbrev-code-everywhere t
	company-dabbrev-code-modes t
	company-dabbrev-code-other-buffers 'all
	company-dabbrev-downcase nil
	company-dabbrev-ignore-case t
	company-dabbrev-other-buffers 'all
	company-require-match nil
	company-show-numbers t
	company-tooltip-limit 20
	company-echo-delay 0
	company-tooltip-offset-display 'scrollbar
        company-tooltip-align-annotations t
        company-idle-delay 0.0
        company-show-numbers t  ;; 给选项编号 (按快捷键 M-1、M-2 等等来进行选择).
        company-selection-wrap-around t
        company-transformers '(company-sort-by-occurrence) ; 根据选择的频率进行排序，读者如果不喜欢可以去掉
	company-begin-commands '(self-insert-command))
  (push '(company-semantic :with company-yasnippet) company-backends)
  :hook ((after-init . global-company-mode)))

;; (use-package company-box
;;   :ensure t
;;   :hook (company-mode . company-box-mode))


;;按键提示
(use-package which-key
  :ensure t
  :defer nil
  :hook (after-init . which-key-mode)
  :config
  (which-key-add-key-based-replacements
    "C-c !" "flycheck"
    "C-c @" "hideshow"
    "C-c i" "ispell"
    "C-c t" "hl-todo"
    "C-x a" "abbrev"
    "C-x n" "narrow"
    "C-x t" "tab")
  :custom
  (which-key-idle-delay 0.5)
  (which-key-add-column-padding 1))


;;文本编辑之行/区域上下移动
;; (use-package drag-stuff
;;   :config
;;   (global-set-key (kbd "M-<down>") #'drag-stuff-down)
;;   (global-set-key (kbd "M-<up>") #'drag-stuff-up)
;;   :bind
;;   (("<M-up>". drag-stuff-up)
;;    ("<M-down>" . drag-stuff-down)))

;;注释
(use-package newcomment
  :ensure nil
  :bind ([remap comment-dwim] . comment-or-uncomment)
  :config
  (defun comment-or-uncomment ()
    "Comment or uncomment the current line or region.
If the region is active and `transient-mark-mode' is on, call
`comment-or-uncomment-region'.
Else, if the current line is empty, insert a comment and indent
it.
Else, call `comment-or-uncomment-region' on the current line."
    (interactive)
    (if (region-active-p)
        (comment-or-uncomment-region (region-beginning) (region-end))
      (if (save-excursion
            (beginning-of-line)
            (looking-at "\\s-*$"))
          (comment-dwim nil)
        (comment-or-uncomment-region (line-beginning-position) (line-end-position)))))
  :custom
  ;; `auto-fill' inside comments.
  ;;
  ;; The quoted text in `message-mode' are identified as comments, so only
  ;; quoted text can be `auto-fill'ed.
  (comment-auto-fill-only-comments t))

;; format all, formatter for almost languages
(use-package format-all
  :diminish
  :hook (prog-mode . format-all-ensure-formatter)
  :bind ("C-c f" . #'format-all-buffer))


;;模板插件
(use-package yasnippet
  :hook (prog-mode . yas-minor-mode))
(use-package yasnippet-snippets
  :after yasnippet)


;; ctrlf, good isearch alternative
(use-package ctrlf
  :hook (after-init . ctrlf-mode))

;; hungry delete, delete many spaces as one
(use-package hungry-delete
  :diminish
  :hook (after-init . global-hungry-delete-mode))

;; move-text, move line or region with M-<up>/<down>
;; (use-package move-text
;;   :hook (after-init . move-text-default-bindings))

(use-package recentf
  :init
  (setq recentf-max-menu-item 10)
  ;;:config
  ;;(recentf-mode 1)
  :hook (after-init . recentf-mode)
  :bind
  (("C-x C-r" . 'recentf-open-files))
  :custom
  (recentf-max-saved-items 300)
  (recentf-auto-cleanup 'never)
  (recentf-exclude '(;; Folders on MacOS start
                     "^/private/tmp/"
                     "^/var/folders/"
                     ;; Folders on MacOS end
                     "^/tmp/"
                     "/ssh\\(x\\)?:"
                     "/su\\(do\\)?:"
                     "^/usr/include/"
                     "/TAGS\\'"
                     "COMMIT_EDITMSG\\'")))


;;输入的历史命令放在最前面
(use-package amx
  :ensure t
  :init (amx-mode))


;;有道翻译
(use-package youdao-dictionary
  :config
  (setq youdao-dictionary-search-history-file "~/.emacs.d/.youdao") ;;设置临时目录
  (setq url-automatic-caching t) ;;启用缓存
  (setq youdao-dictionary-use-chinese-word-segmentation t) ;;支持中文分词
  :bind
  (("C-c y" . 'youdao-dictionary-search-at-point)))

;;快速运行
(use-package quickrun
  :ensure t
  :bind ("C-c x" . quickrun)
  :custom
  (quickrun-focus-p nil)
  (quickrun-input-file-extension ".qr"))

;; Project management
(use-package projectile
  :ensure t
  :hook (after-init . projectile-mode)
  :bind (:map projectile-mode-map
              ("C-c p" . projectile-command-map))
  :config
  (dolist (dir '("bazel-bin"
                 "bazel-out"
                 "bazel-testlogs"))
    (add-to-list 'projectile-globally-ignored-directories dir))
  :custom
  (projectile-use-git-grep t)
  (projectile-indexing-method 'alien)
  (projectile-kill-buffers-filter 'kill-only-files)
  ;; Ignore uninteresting files. It has no effect when using alien mode.
  (projectile-globally-ignored-files '("TAGS" "tags" ".DS_Store"))
  (projectile-globally-ignored-file-suffixes '(".elc" ".pyc" ".o" ".swp" ".so" ".a"))
  (projectile-ignored-projects `("~/"
                                 "/tmp/"
                                 "/private/tmp/"
                                 ,package-user-dir)))

;;辅助线
(use-package highlight-indent-guides
  :custom
  (highlight-indent-guides-method 'character)
  (highlight-indent-guides-responsive 'top)
  (highlight-indent-guides-auto-enabled nil)
  ;;  :custom-face  ; NOTE: The character does not work with "RobotoMono Nerd Font"
  ;;  (highlight-indent-guides-character-face ((t (:family "Source Code Pro" :foreground ,fk/light-color7))))
  ;;  (highlight-indent-guides-top-character-face ((t (:family "Source Code Pro" :foreground ,fk/light-color5))))
  :hook
  (prog-mode . highlight-indent-guides-mode))


(use-package minimap
  :config
  :custom
  (minimap-window-location 'right)
  (minimap-minimum-width 0)
  :hook
  (prog-mode . minimap-mode))

(message "常用包加载完成")
(provide 'init-common-package)
