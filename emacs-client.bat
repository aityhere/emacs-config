﻿@echo off
rem set EMACS_HOME=c:/common-tools/emacs-23.4
rem set HOME=c:/common-tools/emacs-23.4
rem set HOME=D:/GitRepository/emacs-config-git
rem set SBCL_HOME=c:/common-tools/lisp/sbcl
rem set CCL_DEFAULT_DIRECTORY=c:/common-tools/lisp/ccl
rem ccl默认采用USERPROFILE做为用户HOME目录,在此设置环境变量改变CCL的HOME目录,以便和sbcl一起使用quicklisp.
rem set USERPROFILE=%HOME%
rem set EMACS_SERVER_FILE=%HOME%/.emacs.d/server/server
rem set PATH=%PATH%;%EMACS_HOME%/bin;%SBCL_HOME%;%CCL_DEFAULT_DIRECTORY%;%HOME%

rem runemacs --debug-init
RunHiddenConsole.exe "d:/common-tools/emacs/bin/emacsclientw.exe" --alternate-editor="d:/common-tools/emacs/bin/runemacs.exe" "%1"
 
