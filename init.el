;;;; init.el
;;;; author: fengzp
;;;; version: 2.0
;;;; url:
;;;; keyworkds:
;;;; (c) fengzp, 2022
;;;;


(message (concat "emacs版本:" emacs-version))
(message (concat "emacs目录:" user-emacs-directory ))

;;配置文件所在目录
(defconst base-config-dir (file-name-directory load-file-name))
;;设置自定义文件
(setq custom-file (expand-file-name "custom.el" base-config-dir))


;;把自定义的lisp目录加入的load-path
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;;工具方法、变量、常量定义
(require 'init-utils)
;;初始化包管理
(require 'init-package)
;;字体配置
(require 'init-font-config)
;;编码配置
(require 'init-encoding-config)
;;常用设置
(require 'init-common-config)
;;主题
(require 'init-theme-config)
;;状态栏配置
(require 'init-modeline-config)
;;ivy专项配置
(require 'init-ivy-config)
;;侧边栏配置
;;(Require 'init-sidebar)
;;常用插件配置
(require 'init-common-package)
;;版本控制配置
(require 'init-cvs-config)
;;java开发环境配置
(require 'init-development-java)
;;go开发环境配置
(require 'init-development-golang)
;;web开发环境配置
(require 'init-development-web)
;;lisp开发环境配置
(require 'init-development-lisp)

;;VIM按键配置
(require 'init-evil-config)

;;测试基本组件配置
;;(require 'init-test)
