#Emacs配置

``` lang=elisp
;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://my.oschina.net/wuhen86/blog/
;;;; Version: 2.0
;;;; 日期: 2022
;;;; 说明: 1.0的配置至今已经有十几年了，emacs也从23升级到28了，2.0版本的配置更模块化、自动化。
;;;;      在此感谢为emacs默默奉献的emacser。
```

我的emacs配置 感谢网上的emacser们,
在此不一一列出,太多了! 
配置文件中如有版权问题请与我联系
Email:wuhen86@gmail.com

